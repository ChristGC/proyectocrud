const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/',(req, res)=>{
    res.status(200).json({
        message: "este mensaje es desde el servidor"
    })
});
router.get('/panel', async (req, res)=>{
    const panel = [];
    sql="select * from panel";

    let result = await DB.Open(sql,[],false);
    
    //console.log(result.rows);
    console.log(panel)
    result.rows.map(news=>{
        let newsSchema ={
            "id_Panel": news[0],
            "evento": news[1],
            "fecha": news[2],
            "idoperadorpanel": news[3]
            
        }
        panel.push(newsSchema);
    });
    res.json({panel});
});
router.put('/panel', async (req, res)=>{
    const {id_Panel, evento, fecha,idoperadorpanel}= req.body;
 
    sql= "update panel set evento=:evento, fecha=:fecha, idoperadorpanel=:idoperadorpanel where id_Panel=:id_Panel";
 
    await DB.Open(sql, {id_Panel, evento, fecha,idoperadorpanel}, true);
 
    res.status(200).json({
        "id_Panel": id_Panel,
        "evento": evento,
        "fecha": fecha,
        "idoperadorpanel": idoperadorpanel
    })
 
 });

 module.exports = router;