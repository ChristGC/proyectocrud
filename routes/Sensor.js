const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/',(req, res)=>{
    res.status(200).json({
        message: "este mensaje es desde el servidor"
    })
});
router.get('/sensor', async (req, res)=>{
    const sensor = [];
    sql="select * from Sensor";

    let result = await DB.Open(sql,[],false);
    
    //console.log(result.rows);
    console.log(sensor)
    result.rows.map(news=>{
        let newsSchema ={
            "id_sensor": news[0],
            "Estado": news[1],
            "Peso": news[2]
            
        }
        sensor.push(newsSchema);
    });
    res.json({sensor});
});
router.put('/sensor', async (req, res)=>{
    const { id_sensor, estado, peso }= req.body;
 
    sql= "update sensor set estado=:estado, peso=:peso  where id_sensor=:id_sensor";
 
    await DB.Open(sql, {id_sensor, estado, peso}, true);
 
    res.status(200).json({
        "id_sensor": id_sensor,
        "estado": estado,
        "peso": peso
    })
 
 });

 module.exports = router;