const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/', (req, res) => {
    res.status(200).json({
        message: "este mensaje es desde el servidor"
    })
});

router.get('/noticias', async (req, res) => {
    const noticia = [];
    sql = "select * from noticias";

    let result = await DB.Open(sql, [], false);

    //console.log(result.rows);
    console.log(noticia)
    result.rows.map(news => {
        let newsSchema = {
            "id_noticia": news[0],
            "titulo": news[1],
            "informacion": news[2],
            "fecha": news[3],
            "heridos": news[4],
            "fallecidos": news[5],
            "lugar": news[6]
        }
        noticia.push(newsSchema);
    });
    res.json({ noticia });
});

router.post('/noticias', async (req, res) => {
    const { id_noticia, titulo, informacion, fecha, heridos, fallecidos, lugar } = req.body;

    sql = "insert into noticias (id_noticia, titulo, informacion, fecha, heridos, fallecidos, lugar) values (:id_noticia, :titulo, :informacion, :fecha, :heridos, :fallecidos, :lugar)";

    await DB.Open(sql, { id_noticia, titulo, informacion, fecha, heridos, fallecidos, lugar }, true);

    res.status(200).json({
        "id_noticia": id_noticia,
        "titulo": titulo,
        "informacion": informacion,
        "fecha": fecha,
        "heridos": heridos,
        "fallecidos": fallecidos,
        "lugar": lugar
    })

});

router.put('/noticias', async (req, res)=>{
    const { id_noticia, titulo, informacion, fecha, heridos, fallecidos, lugar }= req.body;
 
    sql= "update noticias set titulo=:titulo, informacion=:informacion, fecha=:fecha, heridos=:heridos, fallecidos=:fallecidos, lugar=:lugar where id_noticia=:id_noticia";
 
    await DB.Open(sql, { id_noticia, titulo, informacion, fecha, heridos, fallecidos, lugar }, true);
 
    res.status(200).json({
        "id_noticia": id_noticia,
        "titulo": titulo,
        "informacion": informacion,
        "fecha": fecha,
        "heridos": heridos,
        "fallecidos": fallecidos,
        "lugar": lugar
    })
 
 });

 router.delete('/noticias/:id_noticia', async (req, res)=>{
    const {id_noticia} = req.params;

    sql= "delete from noticias where id_noticia=:id_noticia";

    await DB.Open(sql, {id_noticia}, true);

    res.json({'msg':"Noticia eliminada"})
});

module.exports = router;