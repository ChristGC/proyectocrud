const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/login', async (req, res) => {
    const login = [];
    sql = "select * from login";

    let result = await DB.Open(sql, [], false);

    console.log(login)
    result.rows.map(news => {
        let newsSchema = {
            "id_login": news[0],
            "idoperador": news[1],
            "usuario": news[2],
            "password": news[3]
        }
        login.push(newsSchema);
    });
    res.json({ login });
});

router.put('/login', async (req, res)=>{
    const { id_login, idoperador, usuario, password }= req.body;
 
    sql= "update login set idoperador=:idoperador, usuario=:usuario, password=:password where id_login=:id_login";
 
    await DB.Open(sql, { id_login, idoperador, usuario, password }, true);
 
    res.status(200).json({
        "id_login": id_login,
        "idoperador": idoperador,
        "usuario": usuario,
        "password": password
    })
 
 });

module.exports = router;