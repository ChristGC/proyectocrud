const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/',(req, res)=>{
    res.status(200).json({
        message: "este mensaje es desde el servidor"
    })
});
router.get('/direccion', async (req, res)=>{
    const direccion = [];
    sql="select * from direccion";

    let result = await DB.Open(sql,[],false);
    
    //console.log(result.rows);
    console.log(direccion)
    result.rows.map(news=>{
        let newsSchema ={
            "id_direccion": news[0],
            "nombrecalle": news[1],
            "idtiempo": news[2],
            "idsemaforodir": news[3],
            "idzona": news[4],
            "idestado": news[5]
        }
        direccion.push(newsSchema);
    });
    res.json({direccion});
});
router.put('/direccion', async (req, res)=>{
    const {id_direccion, nombrecalle, idtiempo, idsemaforodir, idzona,idestado}= req.body;
 
    sql= "update direccion set nombrecalle=:nombrecalle, idtiempo=: idtiempo, idsemaforodir=: idsemaforodir, idzona=: idzona, idestado=: idestado  where id_direccion=:id_direccion";
 
    await DB.Open(sql, {id_direccion, nombrecalle, idtiempo, idsemaforodir, idzona,idestado}, true);
 
    res.status(200).json({
        "id_direccion": id_direccion,
        "nombrecalle": nombrecalle,
        "idtiempo": idtiempo,
        "idsemaforodir": idsemaforodir,
        "idzona": idzona,
        "idestado": idestado
    })
 
 });

 module.exports = router;