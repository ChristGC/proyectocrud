const { Router } = require('express');
const router = Router();
const DB = require('../config/oracle');

router.get('/',(req, res)=>{
    res.status(200).json({
        message: "este mensaje es desde el servidor"
    })
});
router.get('/zona', async (req, res)=>{
    const zona = [];
    sql="select * from zona";

    let result = await DB.Open(sql,[],false);
    
    //console.log(result.rows);
    console.log(zona)
    result.rows.map(news=>{
        let newsSchema ={
            "id_zona": news[0],
            "nombre": news[1],
            "idpanel": news[2]
            
        }
        zona.push(newsSchema);
    });
    res.json({zona});
});
router.put('/zona', async (req, res)=>{
    const {id_zona, nombre, idpanel}= req.body;
 
    sql= "update zona set nombre=:nombre, idpanel=:idpanel  where id_zona=:id_zona";
 
    await DB.Open(sql, {id_zona, nombre, idpanel}, true);
 
    res.status(200).json({
        "id_zona": id_zona,
        "nombre": nombre,
        "idpanel": idpanel
    })
 
 });

 module.exports = router;