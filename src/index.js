const express = require('express');
const morgan = require ('morgan');
const cors = require('cors');
const app = express();

const noticia = require('../routes/noticias');
const sensor = require('../routes/Sensor')
const camara = require('../routes/camara')
const operador = require('../routes/operador')
const semaforo = require('../routes/Semaforo')
const tiempo = require('../routes/tiempo')
const estado = require('../routes/estado')
const panel = require('../routes/panel')
const zona = require('../routes/zona')
const login = require('../routes/login')
const direccion = require('../routes/direccion')

/**configuraciones */

app.set('port', 2120);

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(noticia); 
app.use(operador); 
app.use(sensor);
app.use(camara);
app.use(semaforo);
app.use(tiempo);
app.use(estado);
app.use(panel);
app.use(zona);
app.use(login);
app.use(direccion);

app.listen(app.get('port'),()=>{
    console.log("server status 200 on port 2120");
});